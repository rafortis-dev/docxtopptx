package br.com.batistashalom.docxtopptx;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConvertDialog extends JDialog {

	private static final long serialVersionUID = 8822271702621790181L;
	private final JPanel contentPanel = new JPanel();
	static Logger LOGGER = LoggerFactory.getLogger(ConvertDialog.class);
	private JTextField textField;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ConvertDialog dialog = new ConvertDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			LOGGER.error("Erro ao iniciar", e);
		}
	}

	/**
	 * Create the dialog.
	 */
	public ConvertDialog() {
		setResizable(false);
		setTitle("Shalom - Conversão de DOCX para PPTX");
		setBounds(100, 100, 698, 172);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[] {0, 0, 0, 2};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel lblNewLabel = new JLabel("Selecione o arquivo DOCX para extrair as imagens em um pptx");
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.gridwidth = 3;
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = 0;
			contentPanel.add(lblNewLabel, gbc_lblNewLabel);
		}
		{
			JLabel lblArquivoDocx = new JLabel("Arquivo Docx:");
			lblArquivoDocx.setFont(new Font("Tahoma", Font.PLAIN, 14));
			GridBagConstraints gbc_lblArquivoDocx = new GridBagConstraints();
			gbc_lblArquivoDocx.insets = new Insets(0, 0, 0, 5);
			gbc_lblArquivoDocx.anchor = GridBagConstraints.EAST;
			gbc_lblArquivoDocx.gridx = 0;
			gbc_lblArquivoDocx.gridy = 1;
			contentPanel.add(lblArquivoDocx, gbc_lblArquivoDocx);
		}
		{
			textField = new JTextField();
			GridBagConstraints gbc_textField = new GridBagConstraints();
			gbc_textField.insets = new Insets(0, 0, 0, 5);
			gbc_textField.fill = GridBagConstraints.HORIZONTAL;
			gbc_textField.gridx = 1;
			gbc_textField.gridy = 1;
			contentPanel.add(textField, gbc_textField);
			textField.setColumns(10);
		}
		{
			JButton button = new JButton("...");
			button.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					JFileChooser openFile = new JFileChooser();
					openFile.removeChoosableFileFilter(openFile.getChoosableFileFilters()[0]);
					openFile.addChoosableFileFilter(new FileNameExtensionFilter("Arquivos Docx", "docx"));
	                openFile.showOpenDialog(contentPanel);
	                if (openFile.getSelectedFile()!= null)
	                	textField.setText(openFile.getSelectedFile().getAbsolutePath());
				}
			});
			GridBagConstraints gbc_button = new GridBagConstraints();
			gbc_button.gridx = 2;
			gbc_button.gridy = 1;
			contentPanel.add(button, gbc_button);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				final JButton converterButton = new JButton("Converter");
				converterButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						converterButton.setEnabled(false);
						contentPanel.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
						Thread convert = new Thread(new Runnable() {
							public void run() {
								try {
									String fileName = textField.getText();
									if (StringUtils.isEmpty(fileName) || !(new File(fileName)).exists()) {
										JOptionPane.showMessageDialog(contentPanel, "Selecione um arquivo DOCX para converter.");
										return;
									}
									DocxImagesToPptx docxImagesToPptx = new DocxImagesToPptx();
									ByteArrayOutputStream out = docxImagesToPptx.convertToPptx(FileUtils.openInputStream(new File(fileName)));
									File output = new File(fileName.substring(0, fileName.lastIndexOf(".")) + ".pptx");
									if (output.exists()) {
										FileUtils.deleteQuietly(output);
									}
									FileUtils.writeByteArrayToFile(output, out.toByteArray());
									JOptionPane.showMessageDialog(contentPanel, "Conversão finalizada: " + output.getAbsolutePath());
								} catch (ConvertionException | IOException e) {
									LOGGER.error("Erro ao converter o docx", e);
									JOptionPane.showMessageDialog(contentPanel, "Erro ao converter para pptx: " + ExceptionUtils.getStackTrace(e));
								} finally {
									contentPanel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
									converterButton.setEnabled(true);
								}
								
							}
						});
						convert.start();
					}
				});
				converterButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
				buttonPane.add(converterButton);
				getRootPane().setDefaultButton(converterButton);
			}
			{
				JButton fecharButton = new JButton("Fechar");
				fecharButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						System.exit(0);
					}
				});
				fecharButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
				fecharButton.setActionCommand("Cancel");
				buttonPane.add(fecharButton);
			}
		}
	}

}
