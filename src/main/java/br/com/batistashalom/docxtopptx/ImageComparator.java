package br.com.batistashalom.docxtopptx;

import java.util.Comparator;

import org.apache.commons.lang3.math.NumberUtils;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;

public  class ImageComparator implements Comparator<BinaryPart> {
    public int compare(BinaryPart bp1, BinaryPart bp2) {
    	String name1 = bp1.getPartName().getName().replaceAll("[^0-9]", "");
    	String name2 = bp2.getPartName().getName().replaceAll("[^0-9]", "");
        return Integer.compare(NumberUtils.toInt(name1), NumberUtils.toInt(name2));
    }
}
