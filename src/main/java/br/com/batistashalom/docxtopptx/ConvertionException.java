package br.com.batistashalom.docxtopptx;

public class ConvertionException extends Exception {
	private static final long serialVersionUID = 8436100568474340748L;

	public ConvertionException() {
		super();
	}

	public ConvertionException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ConvertionException(String message) {
		super(message);
	}
	
}
