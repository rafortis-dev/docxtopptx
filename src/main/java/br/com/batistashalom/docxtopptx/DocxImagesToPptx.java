package br.com.batistashalom.docxtopptx;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import org.docx4j.openpackaging.packages.OpcPackage;
import org.docx4j.openpackaging.packages.PresentationMLPackage;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.Part;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.PresentationML.MainPresentationPart;
import org.docx4j.openpackaging.parts.PresentationML.SlideLayoutPart;
import org.docx4j.openpackaging.parts.PresentationML.SlidePart;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPart;
import org.docx4j.openpackaging.parts.WordprocessingML.BinaryPartAbstractImage;
import org.pptx4j.jaxb.Context;
import org.pptx4j.pml.Pic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DocxImagesToPptx {
	
	static Logger LOGGER = LoggerFactory.getLogger(DocxImagesToPptx.class);
	
	public ByteArrayOutputStream convertToPptx(InputStream docx) throws ConvertionException{
		try {
			// Load the docx
			WordprocessingMLPackage wordMLPackage = (WordprocessingMLPackage) OpcPackage
					.load(docx);
			PresentationMLPackage presentationMLPackage = (PresentationMLPackage) OpcPackage
					.load(DocxImagesToPptx.class.getResourceAsStream("/base.pptx"));
			MainPresentationPart pp = (MainPresentationPart) presentationMLPackage.getParts().getParts()
					.get(new PartName("/ppt/presentation.xml"));
			SlideLayoutPart layoutPart = (SlideLayoutPart) presentationMLPackage.getParts().getParts()
					.get(new PartName("/ppt/slideLayouts/slideLayout1.xml"));
			int cnt=pp.getSlideCount();
			List<BinaryPart> images = new ArrayList<BinaryPart>();
			for (Entry<PartName, Part> entry : wordMLPackage.getParts().getParts().entrySet()) {				
				if (entry.getValue() instanceof BinaryPartAbstractImage) {
					images.add((BinaryPart) entry.getValue());
				}
			}
			Collections.sort(images, new ImageComparator());
			for(BinaryPart bp: images) {
				cnt++;
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				bp.writeDataToOutputStream(bout);
				LOGGER.info(bp.getPartName().getName());
				inserirSlide(bout, presentationMLPackage, pp, layoutPart, cnt);
			}
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			presentationMLPackage.save(out);
			return out;
		} catch (Exception ex) {
			LOGGER.error("Erro ao converter o documento", ex);
			throw new ConvertionException("Erro ao converter o documento", ex);
		}
	}

	@SuppressWarnings("deprecation")
	private static void inserirSlide(ByteArrayOutputStream bout, PresentationMLPackage presentationMLPackage,
			MainPresentationPart pp, SlideLayoutPart layoutPart, int cnt) throws Exception {
		// OK, now we can create a slide
		
		SlidePart slidePart = PresentationMLPackage.createSlidePart(pp, layoutPart,
				new PartName("/ppt/slides/slide"+ cnt+".xml"));

		// Add image part
		BinaryPartAbstractImage imagePart = BinaryPartAbstractImage.createImagePart(presentationMLPackage, slidePart,
				bout.toByteArray());

		// Add p:pic to slide
		slidePart.getJaxbElement().getCSld().getSpTree().getSpOrGrpSpOrGraphicFrame()
				.add(createPicture(imagePart.getSourceRelationship().getId(), cnt));
	}

	private static Object createPicture(String relId, int cnt) throws Exception {

		// Create p:pic
		java.util.HashMap<String, String> mappings = new java.util.HashMap<String, String>();

		mappings.put("id1", ""+cnt);
		mappings.put("name", "Picture from docx");
		mappings.put("descr", "pic"+cnt+".png");
		mappings.put("rEmbedId", relId);
		mappings.put("offx", Long.toString(0));
		mappings.put("offy", Long.toString(0));
		mappings.put("extcx", Long.toString(12192000L));
		mappings.put("extcy", Long.toString(6858000L));

		return org.docx4j.XmlUtils.unmarshallFromTemplate(SAMPLE_PICTURE, mappings, Context.jcPML, Pic.class);

	}

	private static String SAMPLE_PICTURE = "<p:pic xmlns:a=\"http://schemas.openxmlformats.org/drawingml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\" xmlns:p=\"http://schemas.openxmlformats.org/presentationml/2006/main\"> "
			+ "<p:nvPicPr>" + "<p:cNvPr id=\"${id1}\" name=\"${name}\" descr=\"${descr}\"/>" + "<p:cNvPicPr>"
			+ "<a:picLocks noChangeAspect=\"1\"/>" + "</p:cNvPicPr>" + "<p:nvPr/>" + "</p:nvPicPr>" + "<p:blipFill>"
			+ "<a:blip r:embed=\"${rEmbedId}\" cstate=\"print\"/>" + "<a:stretch>" + "<a:fillRect/>" + "</a:stretch>"
			+ "</p:blipFill>" + "<p:spPr>" + "<a:xfrm>" + "<a:off x=\"${offx}\" y=\"${offy}\"/>"
			+ "<a:ext cx=\"${extcx}\" cy=\"${extcy}\"/>" + "</a:xfrm>" + "<a:prstGeom prst=\"rect\">" + "<a:avLst/>"
			+ "</a:prstGeom>" + "</p:spPr>" + "</p:pic>";
}
